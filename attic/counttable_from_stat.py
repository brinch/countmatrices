# -*- coding: utf-8 -*-
''' Script for compiling count matrices from Patricks mapstat files '''
import os
import pickle
import pandas as pd
import numpy as np


def main():
    ''' Loop through project samples and cat mapping results together '''
    rootdir = ['''/home/projects/cge/analysis/cph_sewage_new_July18/mgmapper_new_run_with_insertSizeOption_June19''',
	       '''/home/projects/cge/analysis/cph_sewage_new_July18/mgmapper_samples_included_in_glob_sewage''',
	       '''/home/projects/cge/analysis/cph_sewage_new_July18/mgmapper_new_run_with_insertSizeOption_March19''',
	       '''/home/projects/cge/analysis/cph_sewage_new_July18/run_Nov18_37Samples''',
	       '''/home/projects/cge/analysis/cph_sewage_new_July18/mgmapper_new_run_with_insertSizeOption_16Nov18''']
	       
    col1 = []
    col2 = []
    i=0
    for adir in rootdir:
      print adir
      for subdir, dirs, files in os.walk(adir, followlinks=True):
	if 'abundance.databases.txt' in files and 'stat' in dirs and 'log' not in subdir and 'ontrol' not in subdir.split('/')[-1] and 'eg' not in subdir.split('/')[-1] and '-126-' not in subdir.split('/')[-1]:
	    i=i+1
	    print i, subdir.split('/')[-1]
	    try:
	      psample = pd.read_csv(subdir+'/stat/positive.strain.ResFinder.txt', sep='\t',
                                 index_col=1, dtype=None, encoding='ISO-8859-1')
	    except:
	      print "No psample"
	      psample = pd.DataFrame({})
	    try:
	      nsample = pd.read_csv(subdir+'/stat/negative.strain.ResFinder.txt', sep='\t',
                                 index_col=1, dtype=None, encoding='ISO-8859-1')
	    except:
	      print "No nsample"
	      nsample = pd.DataFrame({})

	    sample = pd.concat([psample,nsample], sort=False)
	    
	    if sample.empty:
	      print "Here is an error"
	      break


	    colname = subdir.split('/')[-1]
	    sample.rename(columns={'Reads': colname}, inplace=True)
       	    sample.rename(columns={'Size (bp)': 'refLength'}, inplace=True)
	    col1.append(sample[colname])
            col2.append(sample[['refLength', 'Description']])

    print "Number of samples in col1:", len(col1)

    count_table = pd.concat(col1, axis=1, sort=True)
    print "Number of different ResFinder genes:", len(count_table)

    count_table[list(count_table.columns)] = count_table[list(
        count_table.columns)].fillna(0.).astype(int)
    
    pickle.dump(count_table, open("ResFinder_counts.p", "wb"))


    refdata = pd.concat(col2, axis=0, sort=True)
    aggfunc = {'Description': 'first', 'refLength': 'max'}
    refdata = refdata.groupby(refdata.index).aggregate(aggfunc)
    pickle.dump(refdata, open("ResFinder_refdat.p", "wb"))
    print "Number of refdata entries:", len(refdata)

    col = []
    for adir in rootdir:
      for subdir, dirs, files in os.walk(adir, followlinks=True):
	if 'abundance.databases.txt' in files and 'ontrol' not in subdir.split('/')[-1] and 'eg' not in subdir.split('/')[-1] and '-126-' not in subdir.split('/')[-1]:
	  colname = subdir.split('/')[-1]
	  sample = pd.read_csv(subdir+'/abundance.databases.txt', sep='\t',
		      index_col=1, dtype=None, encoding='ISO-8859-1',
		      header=None, names=['Mode', 'Database',
			    'Fraction', 'readCount'])
	  sample.rename(columns={'readCount': colname}, inplace=True)
	  col.append(sample[colname])
	
    print "Number of samples in col:", len(col)
    mappings = pd.concat(col, axis=1, sort=True)
    pickle.dump(mappings, open("mapping_counts.p", "wb"))







if __name__ == "__main__":
  main()
