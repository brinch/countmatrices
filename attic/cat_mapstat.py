# -*- coding: utf-8 -*-
''' Script for compiling count matrices from Patricks mapstat files '''
import os
import pickle
import pandas as pd


def main():
    ''' Loop through project samples and cat mapping results together '''

    samples = [('/home/projects/cge/analysis/' +
	        'cph_sewage_new_July18/mgmapper_new_run_with_insertSizeOption_June19/'),
	       ('/home/projects/cge/analysis/' +
                'cph_sewage_new_July18/mgmapper_new_run_with_insertSizeOption_16Nov18/'),
               ('/home/projects/cge/analysis/' +
                'cph_sewage_new_July18/run_Nov18_37Samples'),
               ('/home/projects/cge/analysis/' +
                'cph_sewage_new_July18/mgmapper_new_run_with_insertSizeOption_March19'),
	       ('/home/projects/cge/analysis/' +
	        'cph_sewage_new_July18/mgmapper_samples_included_in_glob_sewage')
	      ]

    mapstat = []
    refdata = []
    maps = []
    for sample in samples:
        for subdir, dirs, files in os.walk(sample, followlinks=True):
            if 'ontrol' not in subdir.split('/')[-1] and 'eg' not in subdir.split('/')[-1] and '-126-' not in subdir.split('/')[-1]:
		if 'mapstat' in dirs:
		    print subdir.split('/')[-1]
                    sample = pd.read_csv(subdir+'/mapstat/ResFinder.mapstat', sep='\t',
                                         index_col=0, dtype=None, encoding='ISO-8859-1',
                                         header=6)
                    colname = subdir.split('/')[-1]
                    sample.rename(columns={'readCount': colname}, inplace=True)
                    mapstat.append(sample[colname])

                    sample = pd.read_csv(subdir+'/mapstat/ResFinder.refdata', sep='\t',
                                         index_col=0, dtype=None, encoding='ISO-8859-1')
                    refdata.append(sample)

                if 'abundance.databases.txt' in files:
                    colname = subdir.split('/')[-1]
                    sample = pd.read_csv(subdir+'/abundance.databases.txt', sep='\t',
                                         index_col=1, dtype=None, encoding='ISO-8859-1',
                                         header=None, names=['Mode', 'Database',
                                                             'Fraction', 'readCount'])
                    sample.rename(columns={'readCount': colname}, inplace=True)
                    maps.append(sample[colname])

    count_table = pd.concat(mapstat, axis=1, sort=True)
    count_table[list(count_table.columns)] = count_table[list(
        count_table.columns)].fillna(0.).astype(int)
    pickle.dump(count_table, open("ResFinder_counts.p", "wb"))

    refdata = pd.concat(refdata, axis=0, sort=True)
    aggfunc = {'combinedGenomeSize': 'max', 'description': 'first',
               'refLength': 'max'}
    refdata = refdata.groupby(refdata.index).aggregate(aggfunc)
    pickle.dump(refdata, open("ResFinder_refdat.p", "wb"))

    mappings = pd.concat(maps, axis=1, sort=True)
    pickle.dump(mappings, open("mapping_counts.p", "wb"))
    print "Collected ", len(mapstat), "mapstat and refdata files"
    print "Collected ", len(maps), "mappings"
    if len(mapstat) == len(maps):
        print "Everything seems fine"
    else:
        print "Mapstat and Mappings mismatch!"


if __name__ == "__main__":
    main()
