#!/services/tools/anaconda2/4.4.0/bin/python
# -*- coding: utf-8 -*-
''' Script for compiling count matrices from Patricks mapstat files 
    Merge mapstats from samples with multiple sequencing runs
'''

import os
import pickle
import pandas as pd
import numpy as np

def merge_samples(count_table):
  samples = ['85','88','89','90','91','92','93','97','98','99','100','101','102','136','140','181','219','224','225','227']
  nmbr  = [2,2,2,2,2,2,2,2,3,3,2,2,2,2,2,2,2,2,2,2]

  for idx,number in enumerate(samples):
    merger = [samplename for samplename in count_table.columns if ('-'+number+'_' in samplename) or ('-'+number+'rerun' in samplename)]
    if len(merger) != nmbr[idx]:
      print "ERROR in merge_samples: couldn't find the correct samples to merge"
    else:
      count_table['new'] = sum([count_table[i] for i in merger])
      count_table.drop(merger, axis=1, inplace=True)
      count_table.rename(columns={'new':merger[0]}, inplace=True)
  return count_table


def main():
  path = '/home/projects/cge/analysis/CPH_sewage/KMA/output/ResFinder_20190213'
  count_table = pd.DataFrame()
  ref_data = pd.DataFrame()
  collection = []
  
  for _, _, files in os.walk(path, followlinks=False):
    files = [afile for afile in files if 'mapstat' in afile]
  
  for afile in files:
    samplename = afile.split('.mapstat')[0]
    sample = pd.read_csv(path+'/'+afile, header=6, sep='\t',
			 index_col=0, dtype=None, encoding='ISO-8859-1')
   
    sample.rename(columns={'fragmentCount': samplename}, inplace=True)
    collection.append(sample[samplename])

  count_table = pd.concat(collection, axis=1, sort=True)

  print "Count table contains", len(count_table.T), "samples" 
  for name in count_table.index.values:
    count_table.rename(index={name:name.split(' ')[0]}, inplace=True)

  count_table = count_table.fillna(0).astype(int)
  count_table = merge_samples(count_table)
  print "After sample merge:", len(count_table.T)
  pickle.dump(count_table, open("ResFinder_counts.p", "wb"))


  path = '/home/projects/cge/analysis/CPH_sewage/KMA/output/genomic_20190404'
  genomic_table = pd.DataFrame()
  refdata = pd.read_csv('genomic_20190404.refdata', header=1, sep='\t', index_col=0)
  bac=refdata[refdata['superkingdom_name']=='Bacteria']


  for _, _, files in os.walk(path, followlinks=False):
    files = [afile for afile in files if 'mapstat' in afile]

  for afile in files:
    samplename = afile.split('.mapstat')[0]
    sample = pd.read_csv(path+'/'+afile, header=6, sep='\t',
			 index_col=0, dtype=None, encoding='ISO-8859-1')
  
    genomic_table.loc['Genomic', samplename] = sample['fragmentCount'].sum()

    sample['id'] = [i.split(' ')[0] for i in sample.index.values]
    sample = sample[sample['id'].isin(bac.index.values)]
    
    genomic_table.loc['Bacteria', samplename] = sample['fragmentCount'].sum()
    with open(path+'/'+afile) as afile:
          lines = [next(afile) for _ in range(4)]
    genomic_table.loc['Fragments', samplename] = lines[-1].split()[-1]
    
  print "Count table contains", len(genomic_table.T), "samples"
  for name in genomic_table.index.values:
    genomic_table.rename(index={name:name.split(' ')[0]}, inplace=True)

  genomic_table = genomic_table.fillna(0).astype(int)
  genomic_table = merge_samples(genomic_table)
  print "After sample merge:", len(genomic_table.T)
  pickle.dump(genomic_table, open("genomic_counts.p", "wb"))



if __name__ == "__main__":
  main()
